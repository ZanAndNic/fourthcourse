package ru.ChickenOrEgg;

/**
 * Created by Андрей on 06.11.2015.
 * Класс, утверждающий, что перым появилось яйцо.
 *
 * @author Заварин А.Н.
 * @version 1.0
 */

public class EggVoice implements Runnable {
    Thread threadEgg;
    public EggVoice(){
        threadEgg = new Thread(this, "\"Яйцо\"");
        System.out.println( threadEgg);
        threadEgg.start();
    }

    @Override
    public void run() {
        for(int i = 10; i > 0; i--){
            try{
                threadEgg.sleep(1000);
            }
            catch (InterruptedException e){
                System.out.println("Поток - \"Яйцо\" прерван");
            }
            System.out.println("Яйцо");
        }
    }
}
