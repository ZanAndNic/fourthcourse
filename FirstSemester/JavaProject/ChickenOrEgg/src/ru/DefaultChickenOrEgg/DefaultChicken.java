package ru.DefaultChickenOrEgg;

/**
 * Created by Андрей on 06.11.2015.
 * Класс, демонстриющий работу двух потоков(threadEgg и main),
 * в споре: «что было раньше, яйцо или курица?».
 *
 * @author Заварин А.Н.
 * @version 1.0
 *
 */

public class DefaultChicken {
    static DefaultEgg threadEgg;

    public static void main(String[] args){
        threadEgg = new DefaultEgg();
        System.out.println("Спор начат...");
        threadEgg.start();
        //Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
        for(int i = 10; i > 0; i--){
            try{
                Thread.sleep(1000);

            }
            catch (InterruptedException e){
                System.out.println("Главный поток прерван");
            }
            System.out.println("Курица");
        }

        if(threadEgg.isAlive()){
            try{
                threadEgg.join();
            }
            catch (InterruptedException e){
                System.out.println("Поток \"Яйцо\" продолжил работу");
            }
            System.out.println("Первым появилось яйцо! Логично)");
        }
        else {
            System.out.println("Первой появилась курица! Не спорю)");
        }
        System.out.println("Спор закончен!");
    }
}
