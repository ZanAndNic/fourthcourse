package ru.DefaultChickenOrEgg;

/**
 * Created by Андрей on 06.11.2015.
 * Класс, утверждающий, что перым появилось яйцо.
 *
 * @author Заварин А.Н.
 * @version 1.0
 */

public class DefaultEgg extends Thread {

    @Override
    public void run() {
        //Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        for(int i = 10; i > 0; i--){
            try{
                sleep(1000);
            }
            catch (InterruptedException e){
                System.out.println("Поток - \"Яйцо\" прерван");
            }
            System.out.println("Яйцо");
        }
    }
}