package ru.exceptionCircle;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Андрей on 20.10.2015.
 *  Класс описывает окружность с заданной точкой с координатами x, y и радиусом - r
 * Реализованы методы для вычисления: длины окружности, площади окружности, длины дуги окружности,
 * площади кругового сектора, расстояния между центрами окружности, перемещение центра окружности в
 * случайную точку квадрата координатной плоскости с диагональю от [-99;-99] до [99;99], равенства площадей
 * окружностей; проверки: касаются ли окружности в одной точке.
 * Добавлена обработка исключительных ситуаций: радиус меньше 0
 *
 * @author Заварин А.Н.
 * @version 1.0
 */
public class Circle {
    public Scanner input = new Scanner(System.in);
    final Random random = new Random();

    private double x;  //координаты по оси OX
    private double y;  //коордиты по оси OY
    private double r;  //радиус окружности
    private String name;  //имя точки

    Circle(){
        name = "";
        this.x = 0;
        this.y = 0;
        this.r = 0;
    }

     Circle(String name, double x, double y, double r){
        this.name = name;
        this.x = x;
        this.y = y;
        //this.r = r;
        exception(r);
    }

    /**
     * Метод для обработки исключительной ситуации: радиус меньше 0
     *
     * @param r - радиус окружости
     */
    public void exception(double r){
        try{
            if(r < 0) throw new Exception("Радиус не может быть меньше 0");
            this.r = r;
        }
        catch (Exception ex){
            System.out.println(ex.getMessage() + ", значение возвращено по модулю!");
            this.r = Math.abs(r);
        }
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getR() {
        return r;
    }

    public String getName() {
        return name;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setR(double r) {
        this.r = r;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void enterCircle() {
        System.out.print("Введите название центра окружности:");
        name = input.next();
        System.out.print("Введите x :");
        /*String pattern = "[a-zA-Z]*";
        input.skip(pattern);*/
        //if(!input.hasNextDouble()) throw new InputMismatchException("некорректный ввод !!!!111");
        x = input.nextDouble();
        System.out.print("Введите y :");
        y = input.nextDouble();
        System.out.print("Введите радиус окружности:");
        exception(input.nextDouble());
        //r = input.nextDouble();
    }

    @Override
    public String toString(){
        return "Центр окружности точка: " + name + " с координатами x = " + x + " y = " + y + " и радиусом = " + r;
    }
    /**
     *Метод для нахождения длины окружности
     *
     *@return C - длина окружности
     */
    public double circumference(){
        return 2 * Math.PI * r;
    }
    /**
     *Метод для нахождения площади окружности
     *
     *@return S - площадь окружности
     */
    public double squareCircle(){
        return Math.PI * Math.pow(r, 2);
    }
    /**
     *Метод для нахождения длины дуги окружности
     *
     *@param a - угол дуги в градусах
     *@return i - длина дуги окружности
     */
    public double arcLength(double a){
        return (Math.PI * r) / 180 * a;
    }
    /**
     *Метод для нахождения площади кругового сектора
     *
     *@param a - угол сектора круга
     *@return s - площадь коугового сектора
     */
    public double areaOfCircularSector(double a){
        return ((Math.PI * Math.pow(r, 2)) / 360) * a;
    }
    /**
     *Метод сравнения пложадей окружностей
     *
     *@param obj - объект класса
     *@return true - если площади окружностей равны
     *@return false - если площади окружностей неравны
     */
    public boolean comparisonCircles(Circle obj){
        if(squareCircle() == obj.squareCircle()){
            return true;
        }
        else{
            return false;
        }
    }
    /**
     *Метод для перемещения центра круга в случайную точку квадрата
     *координатной плоскости с диагональю от [-99;-99] до [99;99]
     */
    public void moveCenterOfCircle(){
        x = (int)(Math.random()*199) - 99;
        y = (int)(Math.random()*199) - 99;
    }
    /**
     *Метод вычисляющий расстояние между центрами окружностей
     *
     *@param obj - объект класса
     *@return C - расстояние между окружностями
     */
    public double distanceBetweenCenterOfCircle(Circle obj){
        return Math.sqrt(Math.pow(Math.abs(y - obj.y), 2) + Math.pow(Math.abs(x - obj.x), 2));
    }
    /**
     *Метод, проверяющий, касаются ли окружности в одной точке
     *
     *@param obj - объект класса
     *@return true - если окружности касаются в одной точке
     *@return false - если окружности не касаются в одной точке
     */
    public boolean relateToWhetherCircumferenceAtOnePoint(Circle obj){
        double C = distanceBetweenCenterOfCircle(obj);
        if(C == r + obj.r || C == Math.abs(r - obj.r)){
            return true;
        }
        else{
            return false;
        }
    }
}