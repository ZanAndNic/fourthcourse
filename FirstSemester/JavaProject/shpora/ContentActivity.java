package ru.ita.shpora;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ita on 18.01.2016.
 */
public class ContentActivity extends AppCompatActivity {
    TextView contentView;
    private Context context;
    private String[] nameFiles;
    /*private static final String[] NAME_FILES = {
            "OOP",
            "JAVA",
            "Intent"
    };*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_activity);

        contentView = (TextView)findViewById(R.id.txt_content);
        nameFiles = getResources().getStringArray(R.array.names_files);
        int position = getIntent().getIntExtra(MainActivity.KEY_ID, 0);
        String data = "";
        InputFile file = new InputFile(getApplicationContext(), nameFiles[position]);
        data = file.readFile();
        contentView.setText(Html.fromHtml(data));


    }
}
