package ru.ZAN;
/**
 * Created by Андрей on 10.09.2015.
 * Класс, описывающий точку с координатами X, Y и
 * методы для работы с данной точкой
 *
 * @author Заварин А.Н.
 * @version 1.2
 */
public class Point {
    private String name;
    private double x;
    private double y;

    Point(){
        name = "";
        x = 0;
        y = 0;
    }

    Point(String name, double x, double y){
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String  getName() {
        return name;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Точка: "+name+" x = "+x+" y = "+y;
    }
    /**
     * Метод для вычисления расстояния между двуми точками
     *
     * @param b объект класса
     * @return расстояние между двумя точками
     */
    public double howLong(Point b){
        return Math.sqrt(((x - b.getX())*(x - b.getX())) +  ((y - b.getY())*(y - b.getY())));
    }
    /**
     * Метод, определяющий место положение точки в системе координат
     */
    public void quarter(){
        if(x == 0 && y == 0){
            System.out.printf("\nТочка %s находится в начале системы координат координат", name);
        }
        if(x == 0 || y == 0){
            if(x == 0){
                System.out.printf("\nТочка %s лежит на оси X", name);
            }
            else{
                System.out.printf("\nТочка %s лежит на оси Y", name);
            }
        }

        if(x > 0 && y >0){
            System.out.printf("\nТочка %s находится в 1 четверти", name);
        }
        if(x < 0 && y > 0){
            System.out.printf("\nТочка %s находится в 2 четверти", name);
        }
        if(x <0 && y < 0){
            System.out.printf("\nТочка %s находится в 3 четверти", name);
        }
        if(x > 0 && y <0){
            System.out.printf("\nТочка %s находится в 4 четверти", name);
        }
    }
    /**
     * Метод для вычисления середины отрезка
     *
     * @param b объект класса
     */
    public void midpoint(Point b){
        System.out.printf("\nСередина отрезка %s%s с координатами x = %.2f  y = %.2f\n", name, b.getName(), (x + b.getX())/2, (y + b.getY())/2 );
    }
}