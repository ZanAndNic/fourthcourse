package ru.ZAN;

/**
 * Created by Андрей on 10.09.2015.
 * Демо класс демонстрирующий работу класса Point
 *
 * @author Заварин А.Н.
 * @version 1.2
 */
public class Main {
    public static void main(String[] args){
        Point A = new Point();
        Point B = new Point("B", 1, 3);
        Point C = new Point("C", -1, 3);
        Point D = new Point("D", -1, -3);
        Point E = new Point("E", 1, -3);
        Point F = new Point("F", 0, 3);
        Point G = new Point("G", 3, 0);

        System.out.println(B);
        System.out.println(C);
        System.out.println(D);
        System.out.println(E);
        System.out.println(A);
        System.out.println(F);
        System.out.println(G);

        B.quarter();
        C.quarter();
        D.quarter();
        E.quarter();
        A.quarter();
        F.quarter();
        G.quarter();

        System.out.printf("\n\nРасстояние между точкой A и B = %.2f\n", A.howLong(B));
        A.midpoint(B);
    }
}