package ru.ZAN;

/**
 * Created by Андрей on 06.11.2015.
 */

/**
 * Created by Володька on 31.10.2015.
 */
class AffableThread extends Thread
{
    @Override
    public void run()	//Этот метод будет выполнен в побочном потоке
    {
        System.out.println("Привет из побочного потока!");
    }
}

public class Program_1
{
    static AffableThread mSecondThread;

    public static void main(String[] args)
    {
        mSecondThread = new AffableThread();	//Создание потока
        mSecondThread.start();					//Запуск потока


        System.out.println("Главный поток завершён...");
    }
}
