package ru.ZAN;

/**
 * Created by Андрей on 06.11.2015.
 */
public class Chicken extends Thread{

    public static void main(String[] args){
        System.out.println("Спор начат...");
        Egg threadEgg = new Egg();
        for(int i = 10; i > 0; i--){
            try{
                Thread.sleep(1000);
            }
            catch (InterruptedException e){
                System.out.println("Главный поток прерван");
            }
            System.out.println("Курица");
        }

        if(threadEgg.threadEgg.isAlive()){
            try{
                threadEgg.threadEgg.join();
            }
            catch (InterruptedException e){
                System.out.println("Поток продолжил работу");
            }
            System.out.println("Первым появилось яйцо!");
        }
        else{
            System.out.println("Первой появилась курица!");
        }
        System.out.println("Спор закончен!");
    }
}
