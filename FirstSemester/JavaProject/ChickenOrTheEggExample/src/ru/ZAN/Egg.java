package ru.ZAN;

/**
 * Created by Андрей on 06.11.2015.
 */
public class Egg implements Runnable{
    Thread threadEgg;
    public Egg(){
        threadEgg = new Thread(this, "Поток - \"Яйцо\" ");
        threadEgg.start();
    }

    @Override
    public void run() {
        for(int i = 10; i > 0; i--){
            try{
                 threadEgg.sleep(1000);
            }
            catch (InterruptedException e){
                System.out.println("Поток - \"Яйцо\" прерван");
            }
            System.out.println("Яйцо");
        }
    }
}