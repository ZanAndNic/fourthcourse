package ru.resource;

/**
 * Created by Андрей on 12.11.2015.
 * Класс демонстрирующий совместное использование переменной count
 * двумя потоками.
 *
 * @author Заварин А.Н.
 * @version 1.0
 */

public class main {

    public static void main(String[] args) {
        childThread OneThread = new childThread();
        OneThread.setName("Первый");

        childThread TwoThread = new childThread();
        TwoThread.setName("Второй");

        childThread ThreeThread = new childThread();
        ThreeThread.setName("Третий поток");
        OneThread.start();
        /*try {
            OneThread.sleep(10);
        } catch (InterruptedException e) {
            System.out.println("Поток прерван!");
        }*/
        TwoThread.start();
        ThreeThread.start();

        if(OneThread.isAlive()){
            waitThread(OneThread);
        }
        if(TwoThread.isAlive()){
            waitThread(TwoThread);
        }
        if(ThreeThread.isAlive()){
            waitThread(ThreeThread);
        }
        System.out.println("Конечное значение переменной = " + childThread.count);
    }

    public static void waitThread(childThread obj){
        try {
            obj.join();
        } catch (InterruptedException e) {
            System.out.println("Поток продолжил работу!");
        }
    }
}