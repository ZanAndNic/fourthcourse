package ru.resource;

/**
 * Created by Андрей on 12.11.2015.
 * Класс, описывающий создание потока, позволяющего
 * выполнять инкремент переменной count.
 *
 * @author Заварин А.Н.
 * @version 1.0
 */

public class childThread extends Thread {

    public static int count;
    public static synchronized  void counter() {
        count++;
    }

    @Override
    public void run() {
        System.out.println(getName() + " поток" + " запущен!");
        for (int i = 0; i < 1000000; i++) {
           /* try {
                sleep(10);
            } catch (InterruptedException e) {
                System.out.println("Поток прерван");
            }*/
            counter();
            //System.out.println(getName() + " - " + count);
        }
    }
}