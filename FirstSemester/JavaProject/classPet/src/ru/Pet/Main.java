package ru.Pet;

/**
 * Created by Андрей on 06.10.2015.
 * Демо класс, демонстрирующий рабуту класса Pet и
 * его дочерних классов
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18Л
 * @version 1.0
 */
public class Main {
    public static void main (String[] args){

        Pet[] mas = new Pet[4];
        mas[0] = new Cat();
        mas[1]= new Horse();
        mas[2] = new Frog();
        mas[3] = new Snake();

        mas[0].setName("Матильда");
        System.out.println(outputInfo(mas[0]));
        mas[0].setExcellenceCount(10);
        System.out.print("\n");

        mas[1].setName("Flash");
        System.out.println(outputInfo(mas[1]));
        mas[1].setExcellenceCount(10);
        System.out.print("\n");

        mas[2].setName("Sky");
        System.out.println(outputInfo(mas[2]));
        mas[2].setExcellenceCount(10);
        System.out.print("\n");

        mas[3].setName("Miki");
        System.out.println(outputInfo(mas[3]));
        mas[3].setExcellenceCount(11);
        System.out.print("\n");

        batl(mas[0], mas[1]);
        batl(mas[2], mas[3]);
    }
    /**
     *Метод для вывода информации о животном
     *
     *@param obj - объект класса
     *@return строка с информацией о животном
     */
    public static String outputInfo(Pet obj){
           return "Класс: " + obj.getClassName() + "\n" +
                    obj.getName() + " сначала " + obj.move()+ ", потом говорит: " + obj.talk() +
                    " и после этого ест " + obj.eating();
    }
    /**
     *Метод для сравнения переимуществ между двумя животными
     *
     *@param obj_1 - объект класса
     *@return obj_2 - объект класса
     */
    public static void batl(Pet obj_1, Pet obj_2 ){
        if(obj_1.excellenceCount > obj_2.excellenceCount){
            System.out.println(obj_1.name + " превосходит " + obj_2.name + "; " + obj_1.excellenceCount +
            " против " + obj_2.excellenceCount);
        }
        else if(obj_1.excellenceCount < obj_2.excellenceCount){
            System.out.println(obj_2.name + " превосходит " + obj_1.name + "; " + obj_2.excellenceCount +
                    " против " + obj_1.excellenceCount);
        }else{
            System.out.println(obj_1.name + " и " + obj_2.name + " равны " + " - " + obj_1.excellenceCount +
                    " против " + obj_2.excellenceCount);
        }
    }
}