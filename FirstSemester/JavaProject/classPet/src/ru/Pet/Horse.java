package ru.Pet;

/**
 * Created by Андрей on 06.10.2015.
 * Класс, описывающий вошадей
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18Л
 * @version 1.0
 */
public class Horse extends Pet {
    @Override
    String move() {
        return " cкачет ";
    }

    @Override
    String talk() {
        return "  Иго-го-го";
    }

    @Override
    String eating(){
        return " Травку";
    }
}