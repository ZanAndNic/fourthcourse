package ru.Pet;

/**
 * Created by Андрей on 06.10.2015.
 * Класс, описывающий животных
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18Л
 * @version 1.0
 */
public abstract class Pet {

    protected int excellenceCount; //преимужество животного (уровень)
    protected String className = getClass().getName();  //имя класс
    protected String name;  //имя животного

    /**
     *Метод описывающий способ передвижения животного
     *
     *@return строка со способ передвижения животного
     */
    abstract String move();
    /**
     *Метод описывающий язык животного
     *
     *@return строка с примером языка животного
     */
    abstract String talk();
    /**
     *Метод описывающий, что ест животное
     *
     *@return строка с примером, что ест животное
     */
    abstract String eating();


    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public int getExcellenceCount() {
        return excellenceCount;
    }

    public String getName() {
        return name;
    }

    public void setExcellenceCount(int excellenceCount) {
        this.excellenceCount = excellenceCount;
    }
}