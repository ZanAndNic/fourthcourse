package ru.Pet;

/**
 * Created by Андрей on 08.10.2015.
 * Класс, описывающий змей
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18Л
 * @version 1.0
 */
public class Snake extends Pet{
    @Override
    String move() {
        return " ползет";
    }

    @Override
    String talk() {
        return " Шшш-пшш-шшш";
    }

    @Override
    String eating(){
        return  " Мышек";
    }
}