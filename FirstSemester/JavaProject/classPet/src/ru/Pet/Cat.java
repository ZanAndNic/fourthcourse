package ru.Pet;

/**
 * Created by Андрей on 06.10.2015.
 * Класс, описывающий кошек
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18Л
 * @version 1.0
 */
public class Cat extends Pet {
    @Override
    String move() {
        return " крадется";
    }

    @Override
    String talk() {
        return " Мяу-мяу";
    }

    @Override
    String eating(){
        return "Кitekat";
    }
}