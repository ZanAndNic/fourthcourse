package ru.Pet;

/**
 * Created by Андрей on 08.10.2015.
 * Класс, описывающий люгушек
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18Л
 * @version 1.0
 */
public class Frog extends Pet {
    @Override
    String move() {
        return " прыгает";
    }

    @Override
    String talk() {
        return " Ква - ква - ква";
    }

    @Override
    String eating(){
        return "комариков";
    }
}