package ru.ZAN;
import java.util.Scanner;
/**
 * Created by Андрей on 03.09.2015.
 * Вычисление средней, максимальной, минимальной температуры пациентов.
 * Сортировка массива температур по возрастанию и убыванию
 *
 * @author Заварин А.Н.
 * @version 1.0
 */
public class TemperaturePatients {
    static Scanner reader = new Scanner(System.in);

    public static void main(String[] args) {
        double patients[] = inputTemp();
        for (int i = 0; i < patients.length; i++) {
            System.out.printf("Пациент № %d - %4.1f°C\n",i+1, patients[i]);
        }
        int first = 0,  //Левая граница массива patients
                last = patients.length - 1;  //Правая граница массива patients

        if(patients.length == 1){
            System.out.printf("\nСредняя температура = %.1f ", patients[0]);
            System.out.printf("\nМинимальная температура = %.2f \nМаксимальная температура = %.2f \n", patients[0], patients[0]);
        }
        else {
            System.out.printf("\nСредняя температура = %.1f ", mediumTemp(patients));
            System.out.printf("\nМинимальная температура = %.2f \nМаксимальная температура = %.2f \n", minTemp(patients), maxTemp(patients));

            System.out.print("Сортировка по возрастанию:\n");
            patients = quicksortAscending(patients, first, last);
            outputTemp(patients);

            System.out.print("\nСортировка по убыванию:\n");
            quicksortDescending(patients, first, last);
            outputTemp(patients);
        }
    }
    //Метод для заполнения массива
    public static double[] inputTemp(){
        int numberOfPatients;
        do{
            System.out.print("\nВведите кол-во пациентов:");
            numberOfPatients = reader.nextInt();
            if(numberOfPatients <= 0){
                System.out.print("\nКоличество элементов дожно быть больше 0!!!");
            }
        }while (numberOfPatients <= 0);

        double patients[] = new double[ numberOfPatients];
        for (int i = 0; i < numberOfPatients; i++) {
            patients[i]  = 35+(Math.random()*6);
        }
        return patients;
    }
    //Метод для вывода массивай
    public static void outputTemp(double[] patients){
        for (int i = 0; i < patients.length; i++) {
            System.out.printf("%4.1f°C\n", patients[i]);
        }
    }
    //Метод для вычисления средней температуры
    public static  double mediumTemp(double[] patients){
        double averageTemp = patients[0];
        for (int i = 1; i < patients.length; i++) {
            averageTemp += patients[i];
        }
        return averageTemp/patients.length;
    }
    //Метод для вычисления минимальной температуры
    public static double minTemp(double[] patients){
        double minTemp = patients[0];
        for (int i = 1; i < patients.length; i++) {
            if(patients[i]<minTemp){
                minTemp = patients[i];
            }
        }
        return minTemp;
    }
    //Метод для вычисления максимальной температуры
    public static double maxTemp(double[] patients){
        double maxTemp = patients[0];
        for (int i = 1; i < patients.length; i++) {
            if(patients[i] > maxTemp){
                maxTemp = patients[i];
            }
        }
        return maxTemp;
    }
    //Метод для сортировки массива по возрастанию
    public static double[] quicksortAscending(double[] patients, int first, int last){
        double mid, temp;
        int f = first, l = last;
        mid = patients[(f+l)/2];
        do{
            while (patients[f]<mid) f++;
            while (patients[l]>mid) l--;
            if(f<=l){
                temp = patients[f];
                patients[f] = patients[l];
                patients[l] = temp;
                f++;
                l--;
            }
        }while (f<l);
        if(first<l) quicksortAscending(patients, first, l);
        if(f<last) quicksortAscending(patients, f, last);
        return patients;
    }
    //Метод для сортировки массива по убыванию
    public static void quicksortDescending(double[] patients, int first, int last){
        double mid, temp;
        int f = first, l = last;
        mid = patients[(f+l)/2];
        do{
            while (patients[f]>mid) f++;
            while (patients[l]<mid) l--;
            if(f<=l){
                temp = patients[f];
                patients[f] = patients[l];
                patients[l] = temp;
                f++;
                l--;
            }
        }while (f<l);
        if(first<l) quicksortDescending(patients, first, l);
        if(f<last) quicksortDescending(patients, f, last);
    }
}