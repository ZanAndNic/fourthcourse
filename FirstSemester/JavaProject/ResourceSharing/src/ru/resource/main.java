package ru.resource;

/**
 * Created by Андрей on 12.11.2015.
 * Класс демонстрирующий совместное использование переменной count
 * двумя потоками.
 *
 * @author Заварин А.Н.
 * @version 1.0
 */

public class main {

    public static void main(String[] args) {
        System.out.println("Количествово итераций в каждом потоке = 1000000");
        childThread oneThread = new childThread();
        oneThread.setName("Первый");
        childThread twoThread = new childThread();
        twoThread.setName("Второй");

        oneThread.start();
        twoThread.start();

        try {
            oneThread.join();
            twoThread.join();
        } catch (InterruptedException e) {
            System.out.println("Поток прерван!");
        }
        System.out.println("Конечное значение переменной = " + childThread.count);
    }
}