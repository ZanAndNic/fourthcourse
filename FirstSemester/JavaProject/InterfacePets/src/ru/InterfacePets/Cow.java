package ru.InterfacePets;

/**
 * Created by Андрей on 13.10.2015.
 * Метод, описывающий коров.
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18К
 * @version 1.2
 */
public class Cow implements Pet {

    public String talk(){
        return "муу-муу";
    }

    public String getName() {
        return "Зорька";
    }

    public String toString(){
        return  getName() + " говорит: " + talk();
    }
}