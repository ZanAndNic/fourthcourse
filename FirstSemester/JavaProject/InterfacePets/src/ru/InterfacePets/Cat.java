package ru.InterfacePets;

/**
 * Created by Андрей on 13.10.2015.
 * Метод, описывающий кошек.
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18К
 * @version 1.2
 */
public class Cat implements Pet{

    public String talk(){
        return "мяу-мяу";
    }

    public String getName() {
        return "Барсик";
    }

    public String toString(){
        return  this.getName() + " говорит: " + this.talk();
    }
}