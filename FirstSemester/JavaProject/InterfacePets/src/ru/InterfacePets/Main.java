package ru.InterfacePets;

/**
 * Created by Андрей on 13.10.2015.
 * Демо класс демонстрирующий работу интерфейса Pet
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18К
 * @version 1.2
 */
public class Main {
    public static void main(String[] args){
        Cat barsik = new Cat();
        System.out.println(barsik);

        Dog bobik = new Dog();
        System.out.println(bobik);

        Cow zorka = new Cow();
        System.out.println(zorka);

        Chicken tweedy = new Chicken();
        System.out.println(tweedy);
    }
}