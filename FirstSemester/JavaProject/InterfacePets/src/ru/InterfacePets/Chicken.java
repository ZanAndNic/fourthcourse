package ru.InterfacePets;

/**
 * Created by Андрей on 13.10.2015.
 * Метод, описывающий куриц.
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18К
 * @version 1.2
 */
public class Chicken implements Pet{
    
    public String talk(){
        return "ко-коо";
    }

    public String getName() {
        return "Твиди";
    }

    public String toString(){
        return  getName() + " говорит: " + talk();
    }
}