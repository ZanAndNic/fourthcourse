package ru.InterfacePets;

/**
 * Created by Андрей on 13.10.2015.
 * Метод, описывающий собак.
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18К
 * @version 1.2
 */
public class Dog implements Pet{

    public String talk(){
        return "гав-гав";
    }

    public String getName() {
        return "Бобик";
    }

    public String toString(){
        return  getName() + " говорит: " + talk();
    }
}