package ru.InterfacePets;

/**
 * Created by Андрей on 13.10.2015.
 * Интерфейс для работы с животными
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18К
 * @version 1.2
 */
public interface Pet {
    /**
     * Метод устанавливающий имя жиотного
     *
     * @return строка с именем животного
     */
    String getName();
    /**
     * Метод установливающий язык животного
     *
     * @return строка с языком животного
     */
    String talk();

    @Override
    String toString();
}