package ru.ZAN;
/**
 * Created by Андрей on 12.09.2015.
 * Демо класс, демонстрирующий работу класса Circle:
 * вычисление длины, площади окружности, длины дуги окружности,
 * площади кругового сектора оружности, вычисление расстояния между центрами окружности,
 * сравнение площадей окружностей, перемещение центра окружности в случайную точку квадрата
 * координатной плоскости с диагональю от [-99;-99] до [99;99], проверка - касаются ли окружности в одной точке.
 *
 * @author Заварин А.Н.
 * @version 1.1
 */

public class Main {
    public static void main(String[] args){
        Circle A = new Circle("A", 3, 4, 1);
        Circle B = new Circle("B", 7, 4, 1);

        Circle C = new Circle();
        C.enterCircle();
        System.out.println(C);

        System.out.println(A);
        System.out.print("Длина окружности " + A.getName() + " = " + A.circumference());
        System.out.print("\nПлощадь окружности " + A.getName() + " = " + A.squareCircle());
        System.out.print("\nДлина дуги окружности " + A.getName() + " = " + A.arcLength(10) + " угол дуги = " + 10 + "°");
        System.out.print("\nПлощадь кругового сектора окружности " + A.getName() + " = " + A.areaOfCircularSector(10) + " угол сектора окружности = " + 10 + "°\n\n");

        System.out.println(B);
        System.out.print("Длина окружности " + B.getName() + " = " + B.circumference());
        System.out.print("\nПлощадь окружности " + B.getName() + " = " + B.squareCircle());
        System.out.print("\nДлина дуги окружности " + B.getName() + " = " + B.arcLength(10) + " угол дуги = " + 10 + "°" );
        System.out.print("\nПлощадь кругового сектора окружности " + B.getName()  + " = " + B.areaOfCircularSector(10) + " угол сектора окружности = " + 10 + "°\n");

        if(A.comparisonCircles(B)){
            System.out.print("\nПлощади окружностей " + A.getName() + " и " + B.getName() + " равны");
        }
        else{
            System.out.print("\nПлощади окружностей " + A.getName() + " и " + B.getName() + " не равны");
        }

        System.out.print("\nРасстояние между центрами окружностей " + A.getName() + " и " + B.getName() + " = " + A.distanceBetweenCenterOfCircle(B));

        if(A.relateToWhetherCircumferenceAtOnePoint(B)){
            System.out.print("\nОкружность  " + A.getName() + " и " + B.getName() + " касаются в одной точке");
        }
        else{
            System.out.print("\nОкружность  " + A.getName() + " и " + B.getName() + " не касаются в одной точке");
        }

        System.out.print("\n\nВыполнено перемещение центра окружности " + A.getName() + " в случайную точку квадрата\n" +
                "координатной плоскости с диагональю от [-99;-99] до [99;99]:\n");
        A.moveCenterOfCircle();
        System.out.println(A);
    }
}