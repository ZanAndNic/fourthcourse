package ru.ZAN;
import java.io.*;

/**
 * Created by Андрей on 25.10.2015.
 * Класс, демонстрирует считывание номеров билетов из файла Tickets.txt  и
 * запись в файл LuckyTicket.txt номеров счастливых билетов.
 * Счастливым считается шестизначный номер в котором сумма первых трёх цифр
 * совпадает с суммой трёх последних.
 *
 * @version 1.0
 * @author Заварин А.Н.
 */

public class LuckyTicket {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("Tickets.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("LuckyTicket.txt"));
        String string;
        String[] strings;
        int count = 0;
        int[] mas  = new int[6];

        System.out.println("Билеты:");
        bufferedWriter.write("Счастливые билеты: \r\n");
        while ((string = bufferedReader.readLine()) != null) {
            string.replace(" ", "");
            strings = string.split("");
            mas = parseInt(strings, mas);
            if(isLuckyTicket(mas)){
                count++;
                bufferedWriter.write(string + "\r\n");
            }
            System.out.println(string);
        }
        bufferedWriter.write("Количество счастливых билетов = " + count);
        System.out.println("Количество счастливых билетов = " + count);
        bufferedReader.close();
        bufferedWriter.close();
    }
    /**
     * Методя для определения счастливого билета
     *
     * @param mas массив с номером билета
     * @return true если билет счастливый, иначк false
     */
    public static Boolean isLuckyTicket(int[] mas){
        if((mas[0] + mas[1] + mas[2]) == (mas[3] + mas[4] + mas[5])){
            return true;
        }
        return false;
    }

    /**
     * Метод преобразует массив типа string в массив типа int
     *
     * @param strings масиив с номером билета
     * @param mas массив для записи номера билета
     * @return массив с номером типа int
     */
    public static int[] parseInt(String[] strings, int[] mas){
        for(int i = 0; i < strings.length; i++){
            mas[i] = Integer.parseInt(strings[i]);
        }
        return mas;
    }
}