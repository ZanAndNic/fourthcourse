package ru.synchronization;

/**
 * Created by Андрей on 24.11.2015.
 * Класс, описывающий создание потока, и методов для синхронизации потоков
 *
 * @author Заварин А.Н.
 * @version 1.0
 */

public class ThreadSynchronization extends Thread{
    private static boolean ready = false;
    private int mainId;
    private final Object waitObject;
    public static int count = 0;

    public ThreadSynchronization(int mainId, Object waitObject, String name) {
        this.mainId = mainId;
        this.waitObject = waitObject;
        setName(name);
    }

    /**
     * Метод, выполняющий интремент переменной count
     *
     * @param waitObject монитор
     * @param mainId номер потока
     * @param name имя потока
     */
    public static  void increment(Object waitObject, int mainId, String name) {
        synchronized (waitObject) {
            count++;
            System.out.println( name + " - " + count);
            ready = true;
            waitObject.notifyAll();
            while (ready) {
                try {
                    waitObject.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Метод, выполняющий декремент переменной count
     *
     * @param waitObject монитор
     * @param mainId номер потока
     * @param name имя потока
     */
    public static  void decrement(Object waitObject,  int mainId, String name) {
        synchronized (waitObject) {
            while (!ready) {
                try {
                    waitObject.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            count--;
            System.out.println( name + " - " + count);
            ready = false;
            waitObject.notifyAll();
        }
    }

    @Override
    public void run() {
        System.out.println(getName() + " поток запущен");
        for (int i = 0; i < 100; i++) {
            if(mainId == 1){
                increment(waitObject, mainId, getName());
            }
            if(mainId == 2) {
                decrement(waitObject, mainId, getName());
            }
        }
    }
}