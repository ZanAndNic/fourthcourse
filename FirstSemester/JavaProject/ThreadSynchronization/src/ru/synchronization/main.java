package ru.synchronization;

/**
 * Created by Андрей on 24.11.2015.
 * Класс демонстрирующий синхронное использование переменной count
 * двумя потоками.
 *
 * @author Заварин А.Н.
 * @version 1.0
 */
public class main {

    public static void main(String[] arcs){
        System.out.println("Начальное значение переменной count = " + ThreadSynchronization.count);
        Object waitObject = new Object();
        Thread threadOne = new ThreadSynchronization(1, waitObject, "Первый");
        Thread threadTwo = new ThreadSynchronization(2, waitObject, "Второй");

        threadOne.start();
        threadTwo.start();

        try{
            threadOne.join();
            threadTwo.join();
        }
        catch (InterruptedException e){
            System.out.println("Поток прераван!");
        }
        System.out.println("\nКонечное значение счетчика = " + ThreadSynchronization.count);
    }
}