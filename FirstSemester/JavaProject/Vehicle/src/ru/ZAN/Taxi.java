package ru.ZAN;
/**
 * Created by Андрей on 29.09.2015.
 * Класс, описывающий транспотрные средство - такси
 *
 * @author Заварин А.Н.
 * @version 1.0
 */
public class Taxi extends Car {
    private int passengers;
    private int model;

    /**
     * @param passengers количество пассажиров.
     * @param price рыночная стоимость транспорта.
     */
    public Taxi(int passengers, double price) {
        super(200, price, 4);
        this.passengers = passengers;
    }

    public void Message() {
        super.Message();
        System.out.println("This is a message from Taxi class");
    }

    public String toString() {
        return "Taxi: " + super.toString() + ", passengers: " + this.passengers;
    }
}