package ru.ZAN;
/**
 * Created by Андрей on 29.09.2015.
 * Класс, описывающий наземные транспотрные средства
 *
 * @author Заварин А.Н.
 * @version 1.0
 */
public class Car extends Vehicle {
    private int wheels;
    /**
     * @param maxSpeed Максимальная скорость транспорта.
     * @param price рыночная стоимость транспорта.
     * @param wheels количество колес.
     */
    public Car(int maxSpeed, double price, int wheels) {
        super(1, maxSpeed, price);
        this.wheels = wheels;
    }

    /* Вообще не понимаю зачем это нужно, и в частности что такое int vel,
        но на всякий случай переделал, но под комментами

    public Car(Vehicle auto, int wheels) {
        super(auto.getType(), auto.getMaxSpeed());
        this.wheels = wheels;
    }


    public Car(Vehicle auto, int vel, int wheels) {
        super(auto.getType(), vel);
        this.wheels = wheels;
    }
    */

    public void Message() {
        super.Message();
        System.out.println("This is a message from Car class\n");
    }

    public String toString() {
        return "Car: " + super.toString() + ", wheels: " + this.wheels;
    }

}