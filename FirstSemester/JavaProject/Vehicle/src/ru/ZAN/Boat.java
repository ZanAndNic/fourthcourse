package ru.ZAN;
/**
 * Created by Андрей on 29.09.2015.
 * Класс, описывающий водные транспотрные средства
 *
 * @author Заварин А.Н.
 * @version 1.0
 */
public class Boat extends Vehicle {

    /**
     * @param maxSpeed Максимальная скорость транспорта.
     * @param price рыночная стоимость транспорта.
     */
    public Boat(int maxSpeed, double price) {
        super(2, maxSpeed, price);
    }

    public void Message() {
        super.Message();
        System.out.println("This is a message from Boat class\n");
    }

    public String toString() {
        return "Boat: " + super.toString();
    }

}