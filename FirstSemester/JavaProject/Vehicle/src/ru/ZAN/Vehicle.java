package ru.ZAN;
/**
 * Created by Андрей on 29.09.2015.
 * Класс, описывающий наземные, водные и воздушные транспотрные средства
 *
 * @author Заварин А.Н.
 * @version 1.0
 */
public class Vehicle {

    private int type;
    private int maxSpeed;
    private double price;

    /**
     * @param type 1 - Earth, 2 - Water, 3 - Sky.
     * @param maxSpeed Maximum speed of this veh.
     * @param price рыночная стоимость транспорта
     */
    public Vehicle(int type, int maxSpeed, double price) {
        this.type = type;
        this.maxSpeed = maxSpeed;
        this.price = price;
    }

    public void Message(){
        System.out.println("This is a message from Vehicle class");
    }

    public int getType() {
        return this.type;
    }

    public String getTypeAsString(){
        switch (this.type) {
            case 1:
                return "Earth";
            case 2:
                return "Water";
            case 3:
                return "Sky";
        }
        return "";
    }

    public int getMaxSpeed() {
        return this.maxSpeed;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }


    public String toString() {
        return "type: " + this.getTypeAsString() + ", max speed: " + this.maxSpeed + ", price of rental per day = " +
                valuationCar() + " ";
    }
    /**
     * Метод для вычисления стоимости проката транспорта за один день
     *
     * @return стоимость проката транспорта за один день
     */
    public double valuationCar(){
        return Math.round((price * 0.1 * 1.1 * 0.62)/30 * 100.0) / 100.0;
    }
}