package ru.ZAN;
/**
 * Created by ������ on 29.09.2015.
 * ���� �����, ��������������� ������ ������ Vehicle �
 * ��� �������� �������
 *
 * @author ������� �.�.
 * @version 1.0
 */
public class Demo {
    public static void main(String[] args) {

        Airplane plane = new Airplane(1000, 10000, 50000);
        System.out.println(plane.toString());
        plane.Message();

        Boat boat = new Boat(250, 100000);
        System.out.println(boat.toString());
        boat.Message();

        Car car = new Car(200, 900000, 4);
        System.out.println(car.toString());
        car.Message();

        Taxi taxi = new Taxi(3, 300000);
        System.out.println(taxi.toString());
        taxi.Message();

        Truck truck = new Truck(10, 50000);
        System.out.println(truck.toString());
        truck.Message();
    }
}
