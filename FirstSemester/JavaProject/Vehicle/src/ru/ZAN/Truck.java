package ru.ZAN;
/**
 * Created by ������ on 29.09.2015.
 * �����, ����������� �������� ������������ �������� - ��������
 *
 * @author ������� �.�.
 * @version 1.0
 */
public class Truck extends Car{
    private int carrying;

    /**
     * @param carrying ���������������� ����������.
     * @param price �������� ��������� ����������.
     */
    public Truck(int carrying, double price){
        super(120,price, 8);
        this.carrying = carrying;
    }

    public void Message(){
        super.Message();
        System.out.println("This is a message from Truck class");
    }

    public String toString() {
        return "Truck: " + super.toString() + ", carrying: " + this.carrying + " T ";
    }
}