package ru.ZAN;
/**
 * Created by Андрей on 29.09.2015.
 * Класс, описывающий воздушные транспотрные средства
 *
 * @author Заварин А.Н.
 * @version 1.0
 */
public class Airplane extends Vehicle {
    private int maxHeight;

    /**
     * @param maxSpeed Максимальная скорость транспорта.
     * @param maxHeight Максимальная высота транспорта.
     * @param price рыночная стоимость транспорта.
     */
    public Airplane(int maxSpeed, int maxHeight, double price) {
        super(3, maxSpeed, price);
        this.maxHeight = maxHeight;
    }

    public void Message(){
        super.Message();
        System.out.println("This is a message from Airplane class\n");
    }

    public String toString() {
        return "Airplane: " + super.toString() + ", max height: " + this.maxHeight;
    }

}