package ru.ZAN;
import java.util.Random;
/**
 * Created by Андрей on 08.09.2015.
 * Создание, случайное заполнение и вывод массива - массивов
 * заполненногов диапозоне:
 * строки массива: от 1 до 10;
 * столбцы массива: от 1 до 15;
 * значение ячейки массива: от 0,01 до 257.
 *
 * @author Заварин А.Н.
 * @version 1.1
 */
public class Array {
    public double[][] mas;
    final Random random = new Random();
    public Array(){
        this.mas = fillingMas(this.mas);
    }
    //Метод для заполнения массива
    public double[][] fillingMas(double[][] mas){
        mas = new double[random.nextInt(10) + 1][];
        for(int i = 0; i < mas.length; i++){
            mas[i] = new double[random.nextInt(15) + 1];
            for(int j = 0; j < mas[i].length; j++){
                mas[i][j] = random.nextInt(256) + ((double)Math.round(random.nextDouble()*100)/100) + 0.01;
            }
        }
        return mas;
    }
    //Метод для вывода массива
    public void outputMas(){
        System.out.print("Случайно заполненный массив:\n");
        for(int i = 0; i < this.mas.length; i++){
            System.out.printf("  %d - ",i);
            for(int j = 0; j < this.mas[i].length; j++){
                System.out.printf("%.2f |", this.mas[i][j]);
            }
            System.out.print("\n");
        }
    }
}