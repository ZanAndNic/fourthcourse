/**
 * Created by Андрей on 06.10.2015.
 * Вычисление площади:
 *     квадрата;
 *     прямоугольника;
 *     прямоугольного треугольника;
 *     равнобедренного прямоугольного треугольника.
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18К
 * @version 1.0
 */
public class Demo {
    public  static void main(String[] args){
        Rect rect1 = new Rect(2);
        System.out.println("Площадь квадрата: " + rect1.area());

        Rect rect2 = new Rect(10, 5);
        System.out.println("Площадь прямоугольника: " + rect2.area());

        Triangle triangle1 = new Triangle(3);
        System.out.println("Площадь прямоугольного равнобедренного треугольника: " + triangle1.area());

        Triangle triangle2 = new Triangle(5, 10);
        System.out.println("Площадь прямоугольного треугольника: " + triangle2.area());
    }
}