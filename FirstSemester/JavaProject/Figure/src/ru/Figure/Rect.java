/**
 * Created by Андрей on 06.10.2015.
 * Класс для вычисления площади прямоугольника.
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18К
 * @version 1.0
 */
public class Rect extends AbstractF{

    public Rect(double a, double b){
        this.a = a;
        this.b = b;
    }

    public  Rect(double a){
        this(a, a);
    }

    public Rect(){
        this(1);
    }

    @Override
    public double area(){
        return (a * b);
    }
}