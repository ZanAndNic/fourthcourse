/**
 * Created by Андрей on 10.10.2015.
 */
/**
 * Created by Андрей on 06.10.2015.
 * Абстрактный класс для вычисления площади
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18К
 * @version 1.0
 */
public abstract class AbstractF {
    protected double a;
    protected double b;

    abstract double area();
}