/**
 * Created by Андрей on 06.10.2015.
 * Класс для вычисления площади прямоугольного треугольника.
 *
 * @author Заварин А.Н. студент 4 курса группы 12ОИТ18К
 * @version 1.0
 */
public class Triangle extends AbstractF {

    public Triangle(double a, double b){
        this.a = a;
        this.b = b;
    }

    public Triangle(double a){
        this(a, a);
    }

    public Triangle(){
        this(1);
    }

    @Override
    public double area(){
        return (a * b)/2;
    }
}