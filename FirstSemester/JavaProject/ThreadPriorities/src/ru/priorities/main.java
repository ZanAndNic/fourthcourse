package ru.priorities;

/**
 * Created by Андрей on 12.11.2015.
 * Класс демонстрирующий изменение приоритой потока, на примере
 * двух потоков (childThread и main).
 *
 * @author Заварин А.Н.
 * @version 1.0
 */
public class main {

    public static void main(String[] args){
        ChildThread chThread = new ChildThread();
        Thread mainThread = Thread.currentThread();
        System.out.println("Главный поток: - \nДочерный поток: ++++++++++++++++++++++++++++++");
        System.out.println("Текущий приоритет главного потока = " + mainThread.getPriority());
        mainThread.setPriority(10);
        System.out.println("Приоритет главного потока изменен = " + mainThread.getPriority());
        System.out.println("Текущий приоритет дочернего потока = " + chThread.getPriority());
        chThread.setPriority(1);
        System.out.println("Приоритет дочернего потока изменен = " + chThread.getPriority());
        chThread.start();
        counter();
        try{
            chThread.join();
        }
        catch (InterruptedException e){
            System.out.println("Поток прерван!");
        }
    }

    public static void counter(){
        for (int i = 1; i <= 1001; i++){
            try {
                Thread.sleep(5);
            }
            catch (InterruptedException e){
                System.out.println("Главный поток прерван!");
            }
            System.out.println("-");
        }
    }
}
