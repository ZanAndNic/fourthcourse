package ru.priorities;

/**
 * Created by Андрей on 12.11.2015.
 * Класс описавающий создание дочернего потока
 *
 * @author Заварин А.Н.
 * @version 1.0
 */
public class ChildThread extends Thread {

    @Override
    public void run() {
        for (int i = 1; i <= 1001; i++){
            try {
                sleep(5);
            }
            catch (InterruptedException e){
                System.out.println("Дочерний поток прерван!");
            }
            System.out.println("++++++++++++++++++++++++++++++");
        }
    }
}