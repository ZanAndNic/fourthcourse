package com.zanexample.footballcount;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public int countCsk = 0,
               countDynamo = 0;
    public TextView textCsk, textDynamo;
    public Button buttonCsk,
                  buttonDynamo,
                  buttonReset;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null){
            countCsk = savedInstanceState.getInt(COUNT_CSK);
            countDynamo =savedInstanceState.getInt(COUNT_DYNAMO);
        }

        textCsk = (TextView) findViewById(R.id.csk);
        textDynamo = (TextView) findViewById(R.id.dynamo);

        textCsk.setText(Integer.toString(countCsk));
        textDynamo.setText(Integer.toString(countDynamo));

        buttonCsk = (Button) findViewById(R.id.button_csk);
        buttonCsk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countCsk++;
                if(countCsk == 100){
                    buttonCsk.setEnabled(false);
                }
                textCsk.setText(Integer.toString(countCsk));
            }
        });

        buttonDynamo = (Button) findViewById(R.id.button_dynamo);
        buttonDynamo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                countDynamo++;
                if(countDynamo == 100){
                    buttonDynamo.setEnabled(false);
                }
                textDynamo.setText(Integer.toString(countDynamo));
            }
        });

        buttonReset = (Button) findViewById(R.id.reset);
        buttonReset.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                resetCount();
            }
        });

    }
    private static final String COUNT_CSK= "COUNT_CSK";
    private static final String COUNT_DYNAMO= "COUNT_DYNAMO";
    @Override
    public void onSaveInstanceState(Bundle saveInstanceState){
        saveInstanceState.putInt(COUNT_CSK, countCsk);
        saveInstanceState.putInt(COUNT_DYNAMO, countDynamo);

        super.onSaveInstanceState(saveInstanceState);
    }

    public void resetCount(){
        countCsk = 0;
        countDynamo = 0;

        textCsk.setText(Integer.toString(countCsk));
        textDynamo.setText(Integer.toString(countDynamo));

        buttonCsk.setEnabled(true);
        buttonDynamo.setEnabled(true);
    }


/*
    public void tt(Button button, TextView text, int count){

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (count == 100) {
                    buttonDynamo.setEnabled(false);
                }
                textDynamo.setText(Integer.toString(countDynamo));
            }
        });
    }*/
}