﻿window.onload = function(){
    var count = 0;
    function talksAbout(node) {
	    for (var i = 0; i < node.childNodes.length; i++) {
			if(node.childNodes[i].nodeType == document.ELEMENT_NODE){
				count++;
			}
		    talksAbout(node.childNodes[i]);
	    }
    }
	
	talksAbout(document.head);
	talksAbout(document.body);
	
	document.getElementById("output").innerHTML = count;
}