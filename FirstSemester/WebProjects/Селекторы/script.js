$(function(){
    $("#login").on("click", function(){
        $("#login").css("border-color", "#0042FF");
    });

    $("#password").on("click", function(){
        $("#password").css("border-color", "#500DFF");
    });

    $(".content").on("click", function(){
        $(".content").css("background", "#39999C");
    });

    $("#button").on("click", function(){
        setTimeout('second_passed()', 1);
    });
  
    /*Спокойный стиль*/
    $("#select_1").on("click", function(){
        $('body').css({'background':'#A2B2FF'});
	    $(".content").css("background", "#6CFFAD");
	    $("#login").css("border-color", "#FF98D8");
        $("#password").css("border-color", "#FF98D8");
        $("#button").css("background", "#C5E8B0");
        $(".buttonStyle").css("background", "#C5E8B0");
    });
    /*Экспрессивный стиль*/
    $("#select_2").on("click", function(){
        $('body').css({'background':'#FF730E'});
	    $(".content").css("background", "#1B3DFF");
	    $("#login").css("border-color", "#E80A01");
        $("#password").css("border-color", "#E80A01");
        $("#button").css("background", "#D1FF03");
        $(".buttonStyle").css("background", "#D1FF03");
    });
    /*Холодный стиль*/
    $("#select_3").on("click", function(){
        $('body').css({'background':'#4762BF'});
	    $(".content").css("background", "#5F83FF");
	    $("#login").css("border-color", "#2F417F");
        $("#password").css("border-color", "#2F417F");
        $("#button").css({'background':"#182140", 'color':'#fff'});
        $(".buttonStyle").css({"background":"#182140", "color":"#fff"});
    }); 
});
//Возврат к исходному стилю
function second_passed() {
    $("#login, #password").css("border-color", "#A0A0FF");
    $("#cont").css("background", "#FFEC98");
    $('body').css({'background':'white'});
    $("#button").css({"background":"#FFDEDA", "color":"#000"});
    $(".buttonStyle").css({"background":"#FFEEB3", "color":"#000"});
}