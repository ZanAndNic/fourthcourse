//Управление отображением окна на странице(стрыть/показать)
$(document).ready(function(){
	  var flag = true;
    
	//Функция для отображения окна
	function displayWindow(idWindow){
		    if(flag){
			    flag = false;
		        $(idWindow).css("display", "block");
		    }
           var width = jQuery(idWindow).width();
           var height = jQuery(idWindow).height();
        
           var left = (screen.width - width)/2;
           var top = (screen.height - height)/2;
        
           $(idWindow).css({"left": left + "px", "top": top + "px" });
	}
	
	//Функция для скрытия окна
	function closeWindow(idWindow){
		    //Сброс формы
            if(idWindow != ".windowDel" && idWindow != "#_windowBlock_"){
                jQuery('form').get(0).reset();
            }
		    flag = true;
			//Очищаем форму
			$(".file_name p").empty();
			$(".file_name p").append("Файл не выбран");	
			$(".image_uploaded").empty();
           		
		    $(idWindow).css("display", "none");	
	}
	
	/*Удаление вопроса*/
	$("._adminDelObject_").click(function(){
		displayWindow(".windowDel")
	});
	$(".cancelButton").click(function(){
		closeWindow(".windowDel")
	});
	 
	 
	/*Редактирование вопроса*/
	$("._adminEditObject_").click(function(){
		displayWindow("#_windowEditObject_")
	});
	$(".cancelButton").click(function(){
		closeWindow("#_windowEditObject_")
	});
		
	/*Добавление вопроса*/
	$("._adminAddObject_").click(function(){
		displayWindow("#_windowAddObject_")
	});
	$(".cancelButton").click(function(){
		closeWindow("#_windowAddObject_")
	});
 });