﻿/**
 * Created by Андрей on 03.04.2016.
 * Передача данных формы на сервер средствами AJAX
 *
 * Серверная часть
 *
 * @author Заварин А.Н.
 * @version 1.0
 */

var http = require('http').createServer(accept).listen(8080),
    qs = require('querystring'),
    url = require('url'),
    static = require('node-static'),
    file = new static.Server('.'),
    fs = require('fs');

console.log('Setver running at localhost:8080');
var count;

//Считываем кол-во тсж из файла change.txt
fs.readFile('file/change.txt', {encoding: 'utf8'}, function (err, data) {
    if (err) throw err;
    if(data != 0 && data != NaN){
        count = data;
        console.log("Кол-во добавленных ТСЖ: " + count);
    }
    else{
        count = 0;
    }
});

function accept(req, res){
    var masString = new Array(),
        fildNames = new Array(),
        filds = new Array();
    var data = '';

    req.on('data', function (chunk) {
        data += chunk.toString();
        data = qs.parse(data.substring(0, data.length - 1));

        count++;

        //Название полей
        fildNames = [
            "Название организации",
            "\r\nГород",
            "\r\nУлица",
            "\r\nДом",
            "\r\nТелефон",
            "\r\nE-mail",
            "\r\nФамилия председателя",
            "\r\nИмя председателя",
            "\r\nОтчество председателя",
            "\r\nСайт",
            "\r\nРайон"
        ];

        //Имена полей
        filds = [
            'titleTsz',
            'townTsz',
            'streetTsz',
            'houseTsz',
            'phoneNumberTsz',
            'e_mailTsz',
            'surnamePresident',
            'namePresident',
            'patronymicPresident',
            'siteTsz',
            'area'
        ];

        for(var i =0; i<Object.keys(data).length; i++){
            masString[i] = fildNames[i] + ": " + data[filds[i]];
        }

        //Запись тсж в файл
       fs.writeFile('file/tsz_' + count +  '.txt',masString, function(err){
            if(err){
                console.log(err);
            }
            else{
                console.log ('Файл: tsz_' + count +  '.txt - сохранен');
            }
        });

        //Запись количества тсж в файл
       fs.writeFile('file/change.txt',  count, function(err){
            if(err){
                console.log(err);
            }
            else{
                console.log ("Файл change.txt сохранен");
            }
        });

    });

    req.on('end', function () {
        res.end;
    })

    file.serve(req, res);
}