/**
 * Created by Андрей on 03.04.2016.
 * Передача данных формы на сервер средствами AJAX
 *
 * Клиентская часть
 *
 * @author Заварин А.Н.
 * @version 1.0
 */

$(document).ready(function(){

    $('#addTsz').click(function () {
        displayWindow('.windowEditRegistry');
    });

    $('.cancelButton').click(function () {
        closeWindow('.windowEditRegistry');
    });

    $('.delButton').click(function () {
        closeWindow('.windowEditRegistry');
    });

    $(window).resize(function() {
        setScrenSize(".windowEditRegistry");
    });
    
    //Центрирование окна относительно рабочей области браузера
    function setScrenSize(id) {
        var left = ($(window).width()/2-$(id).width()/2);
        var top = ($(window).height()/2-$(id).height()/2);
        $(id).css({"left": left + "px", "top": top + "px" });
    }

    //Отображение окна
    function displayWindow(idWindow) {
        $('.content').append('<div class="pageWindows"></div>');
        $(".pageWindows").css("display", "block");
        $(idWindow).css("display", "block");
        setScrenSize(idWindow);
    }

    //Закрытие окна
    function closeWindow(idWindow){
        $(idWindow).css("display", "none");
        $(".pageWindows").css("display", "none");
        $(".pageWindows").remove();
    }
});