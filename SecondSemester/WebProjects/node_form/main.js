/**
 * Created by Андрей on 03.04.2016.
 * Передача данных формы на сервер средствами AJAX
 *
 * @author Заварин А.Н.
 * @version 1.0
 */

$(document).ready(function () {

    $('#saveForm').click(function () {
        var body ='';
        //Формируем тело запроса
        $("#formAddTsz").find(':input[type="text"], select, textarea').each(function(index, element) {
           // alert(index);
            body += $(this).attr('name') + '=' + encodeURIComponent($(this).val()) + '&';
        });
        vote(body);
   });

    function vote(body){
       var xhr = new XMLHttpRequest();
        xhr.open("POST", '/server.js', true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send(body);
    }
});
