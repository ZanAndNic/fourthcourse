/**
 * Created by Андрей on 20.03.2016.
 * Голосование на тему: "Здоровый образ жизни"
 * Клиентская часть
 *
 * @author Заварин А.Н.
 * @version 2.0
 */

$(document).ready(function () {
    $('.circle').click(function () {
        var idOption = $(this).attr('id');
        vote(idOption);
    })

    function vote(idOption){
        var request = new XMLHttpRequest();
        var params = 'id=' + encodeURIComponent(idOption);
        request.open('GET', '/submit?' + params, true);
        request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                // Success!
                var data = JSON.parse(request.responseText);

                for(var i = 0; i < data.count.length; i++){
                    //Отожражаем кол-во голосов на странице
                    $('#count_' + (i+1)).empty();
                    $('#count_' + (i+1)).append(data.count[i]);

                    //Отображает процентное соотношение
                    $('#progressBar_' + (i+1)).css("width", data.persent[i] + "%");
                    $('#persent_' + (i+1)).empty();
                    $('#persent_' + (i+1)).append(data.persent[i] + "%");
                }

                //Скрываем голосование
                $('.circle').css("display", "none");
                $('.resultOptions').css("display", "block");
                $(".text").css({"margin": "0 0 0 0"});

                //Отображаем
                $('.numberOfUsers').css("display", "block");
                $('#numderOfUsers').empty();
                $('#numderOfUsers').append(data.allCount);

            } else {
                // We reached our target server, but it returned an error
            }
        };
        request.onerror = function() {
            // There was a connection error of some sort
            
        };
        request.send();
       /* var xhr = new XMLHttpRequest();

        var params = 'id=' + encodeURIComponent(idOption);


        alert(params);
        xhr.open('GET', '/submit?' + params, true);

        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.send(null);
        xhr.onreadystatechange = function(){
            //alert(xhr.readyState);
            if(xhr.readyState !=4) return;
            console.log(xhr.status);
            if(xhr.status !=200){
                //Обработать ошибку
                alert('Ошибка ---' + xhr.status + ': ' + xhr.statusTest);
                return;
            }


        }*/

    }
    
    /*function openCount() {
        
    }*/

})
