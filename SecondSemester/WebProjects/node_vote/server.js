﻿/**
 * Created by Андрей on 20.03.2016.
 * Голосование на тему: "Здоровый образ жизни"
 * Серверная часть часть
 *
 * @author Заварин А.Н.
 * @version 2.0
 */


var http = require('http').createServer(accept).listen(8080),
    qs = require('querystring'),
    url = require('url'),
    static = require('node-static'),
    file = new static.Server('.'),
    fs = require('fs');

console.log('Setver running at localhost:8080');

var count = new Array();
var persent = new Array();



fs.readFile('file/test_1.txt', {encoding: 'utf8'}, function (err, data) {
    if (err) throw err;
    if(data != 0){
        count = data.split(',');
    }
    else{
        count[0] = 0;
        count[1] = 0;
        count[2] = 0;
    }
});

function accept(req, res){
    //Если отправлен запрос
    if(req.url.indexOf('id') + 1) {
        var query = url.parse(req.url).query,
            params = qs.parse(query);

        count[params.id-1]++;

        var allCount = 0;

        //Вычисление окщего кол-ва голосов
        for(var i = 0; i < count.length; i++){
            allCount += parseInt(count[i]);
        }

        //Вычесление процентного соотнощения
        for(var i = 0; i < count.length; i++){
            persent[i] = 0;
            persent[i] = Math.round(  (count[i] *100) / allCount * 100) / 100 ;
        }

        fs.writeFile('file/test_1.txt',count, function(err){
            if(err){
                console.log(err);
            }
            else{
                console.log ("Файл сохранен");
            }
        });

        var result = '{"count": ['+ count +'], "persent": ['+ persent +'], "allCount": '+ allCount +'}';

        setTimeout(function(){
            res.end(result);
        }, 500);
    }
    else{
        file.serve(req, res);
    }
}