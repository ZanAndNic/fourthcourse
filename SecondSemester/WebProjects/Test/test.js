describe("pow", function(){
	//1 вариант
	/*it("при возведении 2 в 3 степень  = 8", function(){
		assert.equal(pow(2,3), 8);
	});
	//2 вариант
	/*it("при возведении 3 в 4 степень  = 81", function(){
		assert.equal(pow(3,4), 81);
	});*/
	//3 вариант

	it("При возведение в отрицательную степень результат NaN", function(){
		assert(isNaN(pow(2,-3)), " pow(2,-3) не NaN");
	});

	it("При возведение в дробную степень результат NaN", function(){
		assert(isNaN(pow(2,1.5)), "pow(2,1.5) не NaN" );
	});

	describe("Возведение x в степень n ", function(){
		before(function(){alert("Начало тестов");});
		after(function(){alert("Конец тестов");});

		beforeEach(function(){alert("Вход в тест");});
		afterEach(function(){alert("Выход из теста");});

	  function makeText(x){
	    var expected = x * x * x;
		  it("При возведение " + x + " в степень 3 результат: " + expected, function(){
        assert.equal(pow(x,3), expected);
	    });
	  }
	  for(var x =1; x<=5; x++){
		  makeText(x);
	  }
	});

	describe("Любое число в нулевой степени равно: 1", function(){
		function makeText(x){
			it("При возведении "+ x + "в степень 0 резудьтат: 1", function(){
			  assert.equal(pow(x,0),1);
		  });
		}
		for(var x = -5; x <= 1000000; x+=2){
			makeText(x);
		}
	});

	it("Ноль в нулевой степени дает NaN",
  function(){
		assert(isNaN(pow(0,0)), "pow(0,0) не NaN");
	});

});
