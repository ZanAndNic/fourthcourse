/*Проверка на корректность ввода*/

/*Проверка функции сложения*/
describe("addition", function(){
  describe("Сложение челых, дробных чисел", function(){
    function makeTestAddition(x, y){
	  var result = x + y;
	  it(x +" плюс " + y + " результат: " + result, function(){
		assert.equal(addition(x, y), result);
	  });
	}
	for(var x =-1; x<=1; x+=0.5){
      makeTestAddition(x, 1);
	}
  });
});

/*Проверка функции вычитания*/
describe("subtraction", function(){
  describe("Вычитание челых, дробных чисел", function(){
	function makeTestSubtraction(x, y){
      var result = x - y;
	  it(x + " минус " + y + " результат: "+ result, function(){
		assert.equal(subtraction(x, y), result);
	  });
	}
	for(var x = -1; x<=1; x+=0.5){
	  makeTestSubtraction(x, 1);
	}
  });
});

/*Проверка функции умножения*/
describe("multiplication", function(){
  describe("Умножение челых, дробных чисел", function(){
	function makeTestMultiplication(x, y){
      var result = x * y;
	  it(x + " умножить " + y + " результат: "+ result, function(){
		assert.equal(multiplication(x, y), result);
	  });
	}
	for(var x = -1; x<=1; x+=0.5){
	  makeTestMultiplication(x, 1);
	}
  });
});

/*Проверка функции деления V2.0*/
describe("division", function(){
  describe("Деление челых, дробных чисел", function(){
  function makeTestDivision(x, y){
    var result = x / y;
	  it(x + " разделить на " + y + " результат: "+ result, function(){
	    assert.equal(division(x, y), result);
	  });
	}
	for(var x = -1; x<=1; x+=0.5){
      makeTestDivision(x, 1);
	}
  });
  describe("Обработка исключений", function(){
    it("Проверка деления на положительно числа на 0 (1/0=false)", function(){
	  assert.isFalse(division(1, 0),'division(1,0) результат: false' );
	});
	it("Проверка деления на отрицательное число числа на 0 (-1/0=false)", function(){
      assert.isFalse(division(-1, 0), 'division(1,0) результат: false ');
	});
	it("Проверка деления на 0 на 0 (0/0=false)", function(){
	  assert.isFalse(division(0, 0), 'division(0,0) результат: false ');
	});
  });
});

/*Проверка функции факториала*/
describe("factorial", function(){
  describe("Факториал натуральнах чисел", function(){
    function makeTestFactorial(n){
	  //Берем чило по модулю
	  if(n < 0){
	    Math.abs(n);
	  }
	  var result = 1;
	  for(var i = 1; i<=n; i++){
	    result *= i;
	  }
	  it(n + "! результат: "+ result, function(){
        assert.equal(factorial(n), result);
	  });
	}
	for(var n = 0; n<=5; n+=1){
	  makeTestFactorial(n);
	}
  });
  describe("Обработка исключений", function(){
    it("Проверка на отрицательные числа: -5! = 120", function(){
	  assert.equal(factorial(-2),2);
	});
	it("Проверка на дробные числа 1.2 = false", function(){
	  assert.isFalse(factorial(1.2), 'factorial(1.2) результат: false ');
	});
  });
});