/*Функция сложения*/
function addition(x, y){
  return x+y;
}

/*Функция вычитания*/
function subtraction(x, y){
  return x-y;
}

/*Функция умножения*/
function multiplication(x, y){
  return x*y;
}

/*Функция деления*/
function division(x, y){
  /*Возвращает false, если аргумент является NaN, положительной или отрицательной бесконечностью.
  Иначе возвращает true.*/
  if(isFinite(x/y)){
    return x/y;
  }
  else{
    return false;
  }
}

/*Функция вычисления факториала*/
function factorial(n){
  //Отрицательное число
  if(n%1 != 0){
    return false;
  }
  if(n < 0){
    n = Math.abs(n);
  }
  var result = 1;
  for(var i = 1; i<=n; i++){
    result *= i;
  }
  return result;
}